
#include <Arduino.h>
#include "Encoders.hpp"

#define LED 13


Encoders encoders(2, 4, 3, 5);


void setup()
{
    pinMode(LED, OUTPUT);
    Serial.begin(115200);
    encoders.init();
}


void loop()
{
    digitalWrite(LED, HIGH);
    delay(500);
    digitalWrite(LED, LOW);
    delay(500);

    Serial.print((int32_t)encoders.getLeft());
    Serial.print(F(" - "));
    Serial.print((int32_t)encoders.getRight());
    Serial.println();
}


