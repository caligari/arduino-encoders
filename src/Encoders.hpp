

#ifndef _ENCODERS_HPP
#define _ENCODERS_HPP

#include <stdint.h>

class Encoders
{
    public:

        Encoders(int pin_l_a, int pin_l_b, int pin_r_a, int pin_r_b) :
            _pinLA(pin_l_a),
            _pinLB(pin_l_b),
            _pinRA(pin_r_a),
            _pinRB(pin_r_b),
            _left(0),
            _right(0)
            {}

        void init();

        int64_t getLeft() { return _left; }
        int64_t getRight() { return _right; }

    private:

        int _pinLA;
        int _pinLB;
        int _pinRA;
        int _pinRB;

        volatile int64_t _left;
        volatile int64_t _right;

        static void _isrLeft();
        static void _isrRight();
        static Encoders* _instance;

};


#endif // _ENCODERS_HPP
