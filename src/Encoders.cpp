
#include "Encoders.hpp"

#include <Arduino.h>


static Encoders* Encoders::_instance;


void Encoders::init()
{
    pinMode(_pinLA, INPUT);
    pinMode(_pinLB, INPUT);
    pinMode(_pinRA, INPUT);
    pinMode(_pinRB, INPUT);

    attachInterrupt(digitalPinToInterrupt(_pinLA), _isrLeft, CHANGE);
    attachInterrupt(digitalPinToInterrupt(_pinRA), _isrRight, CHANGE);

    _instance = this;
}


void Encoders::_isrLeft()
{
    if (digitalRead(_instance->_pinLA) == digitalRead(_instance->_pinLB))
        _instance->_left++;
    else
        _instance->_left--;
}


void Encoders::_isrRight()
{
    if (digitalRead(_instance->_pinRA) == digitalRead(_instance->_pinRB))
        _instance->_right++;
    else
        _instance->_right--;
}


